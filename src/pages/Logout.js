import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom';
import AppContext from '../AppContext';

export default function Logout(){

	const { setUser, unsetUser } = useContext(AppContext);

	unsetUser();

	useEffect(() => {

		setUser({
			id: null,
			isAdmin: null
		})

	}, [])


	return(

		<Navigate to="/login"/>
	);
}