import {Row, Col, Container} from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react'
import EmployeeCard from '../components/EmployeeCard';
import Filter from '../components/Filter';
import AppContext from '../AppContext';
import {useParams,useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Employees({employees}) {

	const {user} = useContext(AppContext);
	const [employee, setEmployee] = useState([])


	useEffect(() => {
		fetch('http://localhost:4000/employee/getAllEmployees')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const employeeArr = (data.map(employee => {

				return (
					<EmployeeCard key={employee._id} employeeProp={employee} breakpoint={3}/>
				)

			}))
			setEmployee(employeeArr)
		})

	}, [employee])

	return(
		<>
			<div className = "view">
				<Filter/>
				<div className = "viewContain">
					{employees}
				</div>
			</div>
		</>
	);

};