import { useEffect, useState} from 'react'
import CartCard from '../components/CartCard';



export default function Cart({cart}) {

	const [carts, setCarts] = useState([])


	useEffect(() => {
		fetch(`http://localhost:4000/users/getOrder`, {
			method: 'GET',
			headers: {
			  Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const cartsArr = (data.map(cart => {

				return (
					<CartCard key={cart._id} cartProp={cart} />
				)

			}))
			setCarts(cartsArr)
		})

	}, [cart])

	return(
		<>
			<div>
				<div >
					{carts}
				</div>
			</div>
		</>
	);

};