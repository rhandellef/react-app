import { Button, Form } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import EmployeeTable from '../components/EmployeeTable';
import { useEffect, useState, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import AppContext from '../AppContext';
import Swal from 'sweetalert2';
import Modal from 'react-bootstrap/Modal';
import '../App.css';

export default function AdminDashboard(){
	const {user} = useContext(AppContext);
	const [employees, setEmployees] =useState([])

	// MODAL USESTATE
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [newFirstName, setNewFirstName] = useState('');
	const [newLastName, setNewLastName] = useState('');
	const [newAge, setNewAge] = useState(0);
	const [newEmail, setNewEmail] = useState('');
	const [newBirthdate, setNewBirthdate] = useState('');
	const [newPassword, setNewPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	// console.log(user.isAdmin)
	useEffect(() => {
		fetch('http://localhost:4000/employees/getAllEmployees')
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			setEmployees(data.map(employee => {
				return (
					<EmployeeTable key={employee._id} employeeProp={employee}/>
				)
			}))
		})
		if(newFirstName !== '' && newLastName !== '' && newAge !== 0 && newEmail !== ''  && newBirthdate !== '' && newPassword !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [newFirstName, newLastName, newAge, newEmail,  newBirthdate, newPassword, isActive, show]);


	const addEmployee = (e, employeeId) => {
		e.preventDefault();
		fetch(`http://localhost:4000/employee/add`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: newFirstName,
				lastName:  newLastName,
				age: newAge,
				email: newEmail,
				birthdate: newBirthdate,
				password: newPassword
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.name !== ''){
				Swal.fire({
					title: 'Employee Added successfully',
					icon: 'success',
					text: `${newFirstName} is added to the collection`
				})
				handleClose();
			} else {
				Swal.fire({
					title: 'Employee Addition failed',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
	}


	return(
		(user.id === null || user.isAdmin === false)
		?
		<Navigate to="/adminview"/>
		:
		<>
			<h1 className="styleH1Admin">Admin Dashboard</h1>
			<div className="styleBtnAdmin">
				<Button className="styleBtnAdmin mb-3" onClick={handleShow}>Add New Employee</Button>

			</div>
			<Table striped bordered hover responsive size="sm">
			      <thead className="table-dark">
			        <tr>
			          <th>Firstname</th>
			          <th>Lastname</th>
			          <th>Age</th>
			          <th>Email</th>
			          <th>Birthdate</th>
								<th>Password</th>
			        </tr>
			      </thead>
			      <tbody>
			      	{employees}
			      </tbody>
		    </Table>
	        <>
	          <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
	          <Form onSubmit={ (e) => addEmployee(e)}>
	            <Modal.Header closeButton>
	              <Modal.Title>Add New Employee</Modal.Title>
	            </Modal.Header>
	            <Modal.Body>

	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
	                  <Form.Label>Firstname</Form.Label>
	                  <Form.Control
	                    type="text"
	                    placeholder="New Firstname"
	                    required
	                    onChange={e => setNewFirstName(e.target.value)}
	                    autoFocus
	                  />
	                </Form.Group>
	                <Form.Group
	                  className="mb-3"
	                  controlId="exampleForm.ControlTextarea1"
	                >
	                  <Form.Label>Lastname</Form.Label>
	                  <Form.Control as="textarea" rows={3}
	                  placeholder="New Lastname"
	                  required
	                  onChange={e => setNewLastName(e.target.value)}
	                  />
	                </Form.Group>
	                <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Age</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Age"
	                    required
	                    onChange={e => setNewAge(e.target.value)}
	                  />
	                </Form.Group>
									<Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Email</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Email"
	                    required
	                    onChange={e => setNewEmail(e.target.value)}
	                  />
	                </Form.Group>
									<Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Birthdate</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Birthdate"
	                    required
	                    onChange={e => setNewBirthdate(e.target.value)}
	                  />
	                </Form.Group>
									<Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
	                  <Form.Label>Password</Form.Label>
	                  <Form.Control
	                    type="number"
	                    placeholder="New Password"
	                    required
	                    onChange={e => setNewPassword(e.target.value)}
	                  />
	                </Form.Group>


	            </Modal.Body>
	            <Modal.Footer>
	              <Button variant="secondary" onClick={handleClose}>
	                Close
	              </Button>
	              {isActive
	              	?
		              <Button variant="primary" type="submit">
		                Save Changes
		              </Button>
		              :
		              <Button variant="primary" disabled>
		                Save Changes
		              </Button>
		            }
	            </Modal.Footer>
	            </Form>
	          </Modal>
	        </>
		</>
	)
}
