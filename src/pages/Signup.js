import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Container } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'


export default function Signup() {

    const { user } = useContext(AppContext)

    const history = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [age, setAge] = useState(0);
    const [email, setEmail] = useState('');
    const [birthdate, setBirthdate] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    console.log(password);

    useEffect(() => {
        if(firstName !== '' && lastName !== '' &&  setAge !== ''  && email !== '' && birthdate !== '' && password !== ''){

            setIsActive(true);

        } else {
            setIsActive(false);
        }
    }, [firstName, lastName, age, email, birthdate, password]);

    function registerUser(e) {

        e.preventDefault();

        fetch('http://localhost:4000/users/checkEmailExists', {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'info',
                    text: 'Please provide another email'
                })

            } else {

                fetch('http://localhost:4000/users', {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        age: age,
                        email: email,
                        birthdate: birthdate,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    if(data.email){

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Thank you for registering'
                        })

                        history("/login")

                    } else {

                        Swal.fire({
                            title: 'Registration failed',
                            icon: 'error',
                            text: 'Something went wrong'
                        })
                    }
                })
            }
        })

        setFirstName('');
        setLastName('');
        setAge('');
        setEmail('');
        setBirthdate('')
        setPassword('');
    }

     return (

            user.id !== null ?
            <Navigate to="/courses"/>
            :
            <Container className="w-50">

                <h4 className= "mt-5" style={{
                    display: "flex",
                    justifyContent: "center"
                     }}>
                Sign Up</h4>

                <Card classname="loginCard" class="shadow p-3 mb-5 bg-white rounded">
                    <Card.Body>

                        <Form onSubmit={e => registerUser(e)}>
                            <Form.Group className="mb-3 mt-3" controlId="Firstname">

                                <Form.Control
                                    type="text"
                                    placeholder="Firstname"
                                    required
                                    value={firstName}
                                    onChange={e => setFirstName(e.target.value)}

                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Lastname">

                                <Form.Control
                                    type="text"
                                    placeholder="Lastname"
                                    required
                                    value={lastName}
                                    onChange={e => setLastName(e.target.value)}

                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Age">

                                <Form.Control
                                    type="text"
                                    placeholder="Age"
                                    required
                                    value={age}
                                    onChange={e => setAge(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Email">

                                <Form.Control
                                    type="text"
                                    placeholder="Email"
                                    required
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Birthdate">

                                <Form.Control
                                    type="text"
                                    placeholder="Birthdate"
                                    required
                                    value={email}
                                    onChange={e => setBirthdate(e.target.value)}
                                />
                            </Form.Group>

                            <Form.Group className="mb-3 mt-3" controlId="Password">

                                <Form.Control
                                    type="password"
                                    placeholder="Password"
                                    required
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                />
                            </Form.Group>

                            <div style={{
                                display: "flex",
                                justifyContent: "center"
                                 }}>
                                 { isActive ?
                                     <Button variant="dark" type="submit" id="submitBtn" className="mt-3 mb-5" >
                                         Create Account
                                     </Button>
                                     :
                                     <Button variant="secondary" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
                                         Create Account
                                     </Button>
                                 }

                            </div>

                            <div style={{
                                display: "flex",
                                justifyContent: "center"
                                 }}>
                                <text className="pt-2 ">Already have an account?</text>
                                <Button className="text-primary" variant="muted" as={Link} to="/Login">Login Here!</Button>

                            </div>

                        </Form>

                    </Card.Body>


                </Card>

            </Container>

        )

    }