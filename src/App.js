// import React, {useState, Fragment} from "react";
// import LoginForm from './components/LoginForm';
// import data from "./mock-data.json";
// import { nanoid } from "nanoid";
// import ReadOnlyRow from "./components/ReadOnlyRow";
// import EditableRow from "./components/EditableRow";


// function App () {
//   const adminUser = {
//     email: "admin@admin.com",
//     password: "admin"
//   }

//   const [user, setUser] = useState({name: "", email: "",});
//   const [error, setError] = useState("");

//   const Login = details => {
//     console.log(details);

//     if (details.email == adminUser.email && details.password == adminUser.password){
//       console.log("Logged in");
//       setUser({
//         name: details.name,
//         email: details.email
//       });
//     } else {
//       console.log("Details do not match");
//       setError("Details do not match")
//     }
//   }

//   const Logout = () => {
//     setUser({name: "", email:""});

//   };

//   const [contacts, setContacts] = useState(data);
//   const [addFormData, setAddFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editFormData, setEditFormData] = useState({
//     firstName: "",
//     lastName: "",
//     email: "",
//     age: "",
//     password: "",
//     birthDate: ""
//   });

//   const [editContactId, setEditContactId] = useState(null);

//   const handleAddFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...addFormData };
//     newFormData[fieldName] = fieldValue;

//     setAddFormData(newFormData);
//   };

//   const handleEditFormChange = (event) => {
//     event.preventDefault();

//     const fieldName = event.target.getAttribute("name");
//     const fieldValue = event.target.value;

//     const newFormData = { ...editFormData };
//     newFormData[fieldName] = fieldValue;

//     setEditFormData(newFormData);
//   };

//   const handleAddFormSubmit = (event) => {
//     event.preventDefault();

//     const newContact = {
//       id: nanoid(),
//       firstName: addFormData.firstName,
//       lastName: addFormData.lastName,
//       email:  addFormData.email,
//       age: addFormData.age,
//       password: addFormData.password,
//       birthDate: addFormData.birthDate
//     };

//     const newContacts = [...contacts, newContact];
//     setContacts(newContacts);
//   };

//   const handleEditFormSubmit = (event) => {
//     event.preventDefault();

//     const editedContact = {
//       id: editContactId,
//       firstName: editFormData.firstName,
//       lastName: editFormData.lastName,
//       email: editFormData.email,
//       age: editFormData.age,
//       password: editFormData.password,
//       birthDate: editFormData.birthDate
//     };

//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === editContactId);

//     newContacts[index] = editedContact;

//     setContacts(newContacts);
//     setEditContactId(null);
//   };

//   const handleEditClick = (event, contact) => {
//     event.preventDefault();
//     setEditContactId(contact.id);

//     const formValues = {
//       firstName: contact.firstName,
//       lastName: contact.lastName,
//       email: contact.email,
//       age: contact.age,
//       password: contact.password,
//       birthDate: contact.birthDate
//     };

//     setEditFormData(formValues);
//   };

//   const handleCancelClick = () => {
//     setEditContactId(null);
//   };

//   const handleDeleteClick = (contactId) => {
//     const newContacts = [...contacts];

//     const index = contacts.findIndex((contact) => contact.id === contactId);

//     newContacts.splice(index, 1);

//     setContacts(newContacts);
//   };

//   return (
//     <div className="App">

//       {(user.email != "") ? (
//         <div className="welcome">
//           <h2>Welcome, <span>{user.name}</span></h2>
//           <h2>Add a Contact</h2>
//           <form onSubmit={handleAddFormSubmit}>
//         <input
//           type="text"
//           name="firstName"
//           required="required"
//           placeholder="Enter firstname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="lastName"
//           required="required"
//           placeholder="Enter lastname..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="email"
//           name="email"
//           required="required"
//           placeholder="Enter email..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="age"
//           required="required"
//           placeholder="Enter age..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="password"
//           required="required"
//           placeholder="Enter password..."
//           onChange={handleAddFormChange}
//         />
//         <input
//           type="text"
//           name="birthDate"
//           required="required"
//           placeholder="Enter birthdate..."
//           onChange={handleAddFormChange}
//         />
//         <button type="submit">Add</button>
//       </form>

//       <form onSubmit={handleEditFormSubmit}>
//         <table>
//           <thead>
//             <tr>
//               <th>First Name</th>
//               <th>Last Name</th>
//               <th>Email</th>
//               <th>Age</th>
//               <th>Password</th>
//               <th>Birthdate</th>
//             </tr>
//           </thead>
//           <tbody>
//             {contacts.map((contact) => (
//               <Fragment>
//                 {editContactId === contact.id ? (
//                   <EditableRow
//                     editFormData={editFormData}
//                     handleEditFormChange={handleEditFormChange}
//                     handleCancelClick={handleCancelClick}
//                   />
//                 ) : (
//                   <ReadOnlyRow
//                     contact={contact}
//                     handleEditClick={handleEditClick}
//                     handleDeleteClick={handleDeleteClick}
//                   />
//                 )}
//               </Fragment>
//             ))}
//           </tbody>
//         </table>
//       </form>
//           <button onClick={Logout}>Logout</button>
//         </div>
//       ) : (
//         <LoginForm Login={Login} error={error}/>
//       )}
//     </div>
//   )
// }

// export default App;

import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import NavBar from './components/NavBar';
import Home from './pages/Home';
import Login from './pages/Login';
import Signup from './pages/Signup';
import Admin from './pages/Admin';
import Logout from './pages/Logout'
import SpecificEmployee from './pages/SpecificEmployee'
import EmployeeView from './pages/EmployeeView';
import CartView from './pages/CartView';
import { AppProvider } from './AppContext';


function App() {

	const [user, setUser] = useState({
	    id: null,
	    isAdmin: null
	})

	const unsetUser = () => {
	  localStorage.clear()
	}

	useEffect(() => {

	  fetch('http://localhost:4000/users/getSingleDetails', {
	    method: 'GET',
	    headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    // captured the data of whoever is logged in
	    console.log(data)

	        // set the user states values with the user details upon successful login
	        if(typeof data._id !== "undefined"){
	            setUser({
	                id: data._id,
	                isAdmin: data.isAdmin
	            })
	        } else {

	          // set back the initial state of user
	            setUser({
	                id: null,
	                isAdmin: null
	            })
	        }
	    })

	}, [])

	return(
		<AppProvider value={{user, setUser, unsetUser}}>
			<Router>
				<NavBar/>
				<Container>
					<Routes>
						<Route exact path ="/" element={<Home/>}/>
						<Route exact path ="/home" element={<Home/>}/>
						<Route exact path ="/login" element={<Login/>} />
						<Route exact path ="/signup" element={<Signup/>}/>
						<Route exact path ="/adminview" element={<Admin/>}/>
						<Route exact path ="/employeeview" element={<EmployeeView/>}/>
						<Route exact path ="/viewEmployee/:employeeId" element={<SpecificEmployee/>}/>
						<Route exact path ="/cart/:userId" element={<CartView/>}/>
						<Route exact path ="/logout" element={<Logout/>}/>

					</Routes>
				</Container>
			</Router>
		</AppProvider>
	);

}

export default App;

