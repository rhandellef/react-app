import { useState, useEffect, useContext } from 'react'
import { Card, Col, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'


export default function EmployeeCard(props) {

   const { user } = useContext(AppContext);
   const{employeeId} = useParams();

    //object destructuring
    const {breakpoint, employeeProp} = props
    const {firstName, lastName, age, email, birthdate, password, _id} = employeeProp



    return (
        <Col xs={12} md={breakpoint} className="mt-4" >
        <Card className="card1">
            <Card.Body>
                <Card.Title className="text-center card2">{firstName}</Card.Title>
                <Card.Subtitle>Lastname:</Card.Subtitle>
                <Card.Text className="card3">{lastName}</Card.Text>
                <Card.Subtitle>Age:</Card.Subtitle>
                <Card.Text>{age}</Card.Text>
                <Card.Subtitle>Email:</Card.Subtitle>
                <Card.Text>{email}</Card.Text>
                <Card.Subtitle>Birthdate:</Card.Subtitle>
                <Card.Text>{birthdate}</Card.Text>
                <Card.Subtitle>Password:</Card.Subtitle>
                <Card.Text>{password}</Card.Text>

            </Card.Body>
            <Card.Footer>
                            { user.id !== null
                                ?
                                <div className= "d-grid gap 2">
                                    <Button as={Link} to={`/viewEmployees/${_id}`} className= "btn-primary mb-2">
                                        View
                                    </Button>

                                </div>
                                :
                                <>
                                    <p>Not yet registered? <Link to="/register">Register Here</Link></p>
                                    <Link className="btn btn-danger" to="/login">Log In</Link>
                                </>
                            }
                        </Card.Footer>

        </Card>
        </Col>

    )
}