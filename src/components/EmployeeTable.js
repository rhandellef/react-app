// import Table from 'react-bootstrap/Table';
import { Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
// MODAL IMPORTS
import React, { useState, useEffect } from 'react';
import Modal from 'react-bootstrap/Modal';

export default function EmployeeTable({employeeProp}) {
	// console.log(productProp);
	const {firstName, lastName, age, email, birthdate, password, _id, quantity} = employeeProp;

	// MODAL USESTATE
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const [newFirstName, setNewFirstName] = useState('');
	const [newLastName, setNewLastName] = useState('');
	const [newAge, setNewAge] = useState(0);
	const [newEmail, setNewEmail] = useState('')
	const [newBirthdate, setNewBirthdate] = useState('');
	const [newPassword,  setNewPassword] = useState('');


	const [isActive, setIsActive] = useState(false);


	useEffect(() => {
		if(newFirstName !== '' && newLastName !== '' && newAge !== 0 && newEmail !== ''  && newBirthdate !== '' && newPassword !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [newFirstName, newLastName, newAge, newEmail, newBirthdate, newPassword]);



	const updateEmployee = (e, employeeId) => {
		e.preventDefault();
		fetch(`http://localhost:4000/employee/updateEmployee/${employeeId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: newFirstName,
				lastName: newLastName,
				age: newAge,
				email: newEmail,
				birthdate: newBirthdate,
				password: newPassword

			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.firsName !== firstName || data.LastName !== lastName || data.age !== age || data.email  !== email  || data.birthdate !== birthdate || data.password !== password) {
				Swal.fire({
					title: 'Employee Update successful',
					icon: 'success',
					text: 'Modifications saved'
				})
				// setProdUpdate(true)
				// setProdUpdate(false)
				handleClose();
			} else {
				Swal.fire({
					title: 'Employee Update failed',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
	}

	const archiveEmployee = (e, employeeId) => {
		e.preventDefault();
		fetch(`http://localhost:4000/employee/deactEmployee/${employeeId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: 'Deactivation Successful',
					icon: 'success',
					text: `${firstName} is no longer in stock`
				})
				// setDisabled(true);

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}

	const activateEmployee = (e, employeeId) => {
		fetch(`http://localhost:4000/employee/activateEmployee/${employeeId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: 'Activation Successful',
					icon: 'success',
					text: `${firstName} is now in stock`
				})
				// setDisabled(false);

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	}

    return (
    	<>
        <tr className="styleProdTab">
          <td>{firstName}</td>
          <td>{lastName}</td>
          <td>{age}</td>
          <td>{email}</td>
					<td>{birthdate}</td>
					<td>{password}</td>
          <td>
          	<div className="Topbtn text-center">
	          	<Button className="button btn-primary" onClick={handleShow}>Update</Button>
	          		{quantity
	          		?
	          		<Button className="button btn-danger mx-2" onClick={(e) => archiveEmployee(e, _id) }>Disable</Button>
	          		:
	          		<Button className="button btn-success mx-2" onClick={(e) => activateEmployee(e, _id)}>Activate</Button>
	          	}

	        </div>
          </td>
        </tr>
        <>
          <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false} centered>
          <Form onSubmit={ (e) => updateEmployee(e, _id)}>
            <Modal.Header closeButton>
              <Modal.Title>Update Employee Details</Modal.Title>
            </Modal.Header>
            <Modal.Body>

                <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                  <Form.Label>Firstname</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder={firstName}
                    required
                    onChange={e => setNewFirstName(e.target.value)}
                    autoFocus
                  />
                </Form.Group>
                <Form.Group
                  className="mb-3"
                  controlId="exampleForm.ControlTextarea1"
                >
                  <Form.Label>Lastname</Form.Label>
                  <Form.Control as="textarea" rows={3}
                  placeholder={lastName}
                  required
                  onChange={e => setNewLastName(e.target.value)}
                  />
                </Form.Group>
                <Form.Group className=" mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Age</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={age}
                    required
                    onChange={e => setNewAge(e.target.value)}
                  />
                </Form.Group>
								<Form.Group className=" mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={email}
                    required
                    onChange={e => setNewEmail(e.target.value)}
                  />
                </Form.Group>
								<Form.Group className=" mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Birthdate</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={birthdate}
                    required
                    onChange={e => setNewBirthdate(e.target.value)}
                  />
                </Form.Group>
								<Form.Group className=" mb-3" controlId="exampleForm.ControlInput2">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder={password}
                    required
                    onChange={e => setNewPassword(e.target.value)}
                  />
                </Form.Group>

            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {isActive
              	?
	              <Button variant="primary" type="submit">
	                Save Changes
	              </Button>
	              :
	              <Button variant="primary" disabled onClick={handleClose}>
	                Save Changes
	              </Button>
	            }
            </Modal.Footer>
            </Form>
          </Modal>
        </>
      </>
    )
}
