import React from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ArchiveEmployee({employee,isActive,fetchData}){

    const archiveToggle = (employeeId) => {
        fetch(`http://localhost:4000/employee/${employeeId}/archive`, {
            method:'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
                    title:'Success',
                    icon:'success',
                    text:'Employee Successfully Deactivated'
                })
                fetchData();
            } else {
                Swal.fire({
                    title:'Error',
                    icon:'error',
                    text:'ERROR: Something went wrong, Please try again'
                })
                fetchData();
            }
        })
    }
    const unArchiveToggle = (employeeId) => {
        fetch(`http://localhost:4000/employee/${employeeId}/activate`, {
            method:'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
                    title:'Success',
                    icon:'success',
                    text:'Employee Successfully Activated'
                })
                fetchData();
            } else {
                Swal.fire({
                    title:'Error',
                    icon:'error',
                    text:'ERROR: Something went wrong, Please try again'
                })
                fetchData();
            }
        })
    }

    return(
        <>
            {isActive ?
                <Button variant="danger" size="sm" onClick={() => archiveToggle(employee)}>Archive</Button>
                :
                <Button variant="success" size="sm" onClick={() => unArchiveToggle(employee)}>Unarchive</Button>
            }
        </>
    )
}
