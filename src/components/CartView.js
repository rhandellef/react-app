import { Button, Form } from 'react-bootstrap';
import { useState } from 'react';
import Swal from 'sweetalert2';

export default function CartTable({employeeProp}) {
	console.log(employeeProp)

	const {employeeFirstName, employeeId, price, _id, inStock, quantity} = employeeProp;

	let [count, setCount] = useState(quantity);

	function incrementCount() {
	    count = count + 1;
	    setCount(count);
  }
  function decrementCount() {
      count = count - 1;
      setCount(count);
  }

  let subtotal = (price * count)

  const deleteOrder = (e, employeeId) => {
  	e.preventDefault();
  	fetch(`http://localhost:4000/users/order`, {
  		method: 'PUT',
  		headers: {
  			Authorization: `Bearer ${localStorage.getItem('token')}`
  		},
      body: JSON.stringify({
        employeeId: employeeId
      })
  	})
  	.then(res => res.json())
  	.then(data => {
  		console.log(data);

  		if(data){
  			Swal.fire({
  				title: 'Item Removed',
  				icon: 'success',
  				text: `${employeeFirstName} is no longer in your cart`
  			})

  		} else {
  			Swal.fire({
  				title: 'Something went wrong',
  				icon: 'error',
  				text: 'Please try again later'
  			})
  		}
  	})
  }

	return (
    	<>
        <tr className="styleProdTab">
          <td>{employeeFirstName}</td>
          <td className="alignCenter">${price}</td>
          <td>
    		    <div className="quantity">
    		    	<button onClick={decrementCount}>-</button>
    		      <div>{count}</div>
    		      <button onClick={incrementCount}>+</button>
    	      </div>
          </td>
          <td className="alignCenter">${subtotal}</td>
          <td className="alignCenter">
          	<Button className="btn-danger" onClick={(e) => deleteOrder(e, employeeId)}>Delete</Button>
          </td>

        </tr>
      </>
    )
}
