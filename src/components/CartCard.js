import {useContext } from 'react';
import { Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import AppContext from '../AppContext';
import Swal from 'sweetalert2';


export default function CartView(props) {


    const { user } = useContext(AppContext);


    const {cartProp} = props
    const {employeeFirstName, employeeLastName, employeeAge, employeeEmail, employeeBirthdate, employeePassword, _id} = cartProp

    const deleteOrder = (id)=>{

            fetch(`http://localhost:4000/users/deleteOrder/${user.id}/${_id}`, {

                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);


                if(data) {
                    Swal.fire({
                        title: 'Successfully Deleted',
                        icon: 'success'

                    })

                }else {
                    Swal.fire({
                        title:'Something went wrong',
                        icon: 'error'

                    })
                }
            })
        }


    return(
        <>

            <Table striped bordered hover responsive size="sm">
                  <thead className="table-dark">
                    <tr>
                      <th className="alignCenter">Firstname</th>
                      <th className="alignCenter">Lastname</th>
                      <th className="alignCenter">Age</th>
                      <th className="alignCenter">Email</th>
                      <th className="alignCenter">Birthdate</th>
                      <th className="alignCenter">Password</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                        <td> {employeeFirstName}</td>
                        <td> {employeeLastName}</td>
                        <td> {employeeAge}</td>
                        <td> {employeeEmail}</td>
                        <td> {employeeBirthdate}</td>
                        <td> {employeePassword}</td>
                        <td>
                            <Button onClick={ async() =>{
                             await deleteOrder(_id)
                             window.location.reload()
                            }}>Delete</Button>
                        </td>
                    </tr>
                  </tbody>
            </Table>

        </>


    )

}